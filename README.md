# YooTube

Youtube UI clone.

# What is it?

Implementation of [The Odin's HTML Project](https://www.theodinproject.com/courses/html5-and-css3/lessons/embedding-images-and-video), in HTML & SCSS.

# See the webpage

Start a web server on your local machine, and serve the contents of the `public` folder of this project.

# Run with Docker

You will need a GitLab account to run this with Docker.

After you have done that, follow these steps:

## Log in to GitLab's Docker Registry

Log in to the Registry using the following command:

`$ docker login registry.gitlab.com`

## Serve the site

Run  `$ docker-compose up`, and head over to [your browser](http://localhost:3000) to see the site.